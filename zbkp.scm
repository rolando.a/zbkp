#! /usr/bin/guile -s
!#

(define-module (zbkp))

(use-modules (ice-9 popen))
(use-modules (ice-9 textual-ports))
(use-modules (ice-9 optargs))
(use-modules (ice-9 regex))
(use-modules (ice-9 format))
(use-modules (srfi srfi-19))

(define (zfs-add-entry-to-bucket buckets key entry)
  (zfs-update-bucket buckets key (append (assoc-ref buckets key) (list entry))))

(define (zfs-update-bucket buckets key bucket-content)
  (assoc-set! buckets key bucket-content))

(define (zfs-get-bucket buckets key)
  (assoc-ref buckets key))

(define (zfs-trim-bucket buckets key limit force)
  (let ((bucket (zfs-get-bucket buckets key)))
	(format #t "will apply limit ~a to bucket ~a\n" limit key)
	(cond
	 ((> (length bucket) limit)
	  (zfs-update-bucket buckets key (zfs-delete-last-entry bucket force))
	  (zfs-trim-bucket buckets key limit force)))))

(define (zfs-delete-last-entry bucket force)
  (let* ((last (car bucket))
		 (rest (cdr bucket))
		 (name (assoc-ref last 'name))
		 (backed-up (assoc-ref last 'backed-up)))
	(if (or backed-up force)
		(zfs-run-command (string-append "destroy " name)))
	(format #t "will remove entry ~a\n" name)
	rest))

(define* (zfs-run-command cmd #:optional (ssh-string ""))
  "run a zfs command through an open-pipe, capture output and convert
to a list of lines, and then split each line by tabs."
  (let* ((result (zfs-run-command-ll (format #f "~a sudo zfs ~a" ssh-string cmd)))
		 (output (cdr result))
		 (success (car result)))
	(if success
		(zfs-clean-output output)
		#f)))

(define (zfs-can-connect ssh-string)
  "simple check to see if we can connect to a remote host"
  (let* ((result (zfs-run-command-ll (format #f "timeout 10 ~a echo xxx" ssh-string)))
		 (output (string-trim-right (cdr result) char-set:whitespace))
		 (success (car result)))
	(and success (string=? output "xxx"))))

(define (zfs-run-command-ll cmd)
  "Executes a command and checks for the exit status. Returns a pair
with #t as the car if it was successful, #f if the exit status was not
0. The cdr of the pair is the string output of the command."
  (let* ((port (open-input-pipe cmd))
		 (str (get-string-all port))
		 (exit-code (status:exit-val (close-pipe port))))
	(if (eqv? 0 exit-code) `(#t . ,str) '(#f . ""))))

(define (zfs-clean-output output)
  (filter (lambda (row) (> (length row) 1))
		  (map (lambda (row) (string-split row #\tab))
			   (string-split output #\newline))))

(define* (zfs-get-all-snapshots dataset #:optional (ssh-string ""))
  (let ((output (zfs-run-command
				 (format #f "list -rHt snapshot ~a" dataset) ssh-string))
		(buckets (map list '(hourly daily monthly yearly))))
	(for-each
	 (lambda (row)
	   ;; the first item is the name, try to match it to our pattern:
	   ;; <dataset>@autosnap_<timestamp>_<bucket>
	   (let* ((md (zfs-match-snapshot-name (car row)))
			  (snapshot (and md (zfs-parse-snapshot-name dataset (car row) ssh-string))))
		 (if snapshot (zfs-add-entry-to-bucket buckets (assoc-ref snapshot 'bucket) snapshot))))
	 output)
	buckets))

(define (zfs-match-snapshot-name str)
  (string-match "([A-Za-z0-9/]+)@(autosnap_([0-9]+)_(hourly|daily|monthly))" str))

(define* (zfs-parse-snapshot-name dataset str ssh-string)
  "parse the name of a snapshot and only get additional metadata if
the ssh-string is empty. If the ssh-string is not empty, the backed-up
property will always be #t (we assume it's a backup entry)."
  (let* ((md (zfs-match-snapshot-name str))
		 (name (and md (match:substring md 0))))
	(if (and md (string=? dataset (match:substring md 1)))
		`((name . ,name)
		  (snap-name . ,(match:substring md 2))
		  (dataset . ,dataset)
		  (bucket . ,(string->symbol (match:substring md 4)))
		  (used . ,(if (string=? ssh-string "") (zfs-get-used name "") 0))
		  (backed-up . ,(if (string=? ssh-string "") (zfs-get-backed-up name "") #t))
		  (creation . ,(if (string=? ssh-string "") (zfs-get-creation name "") 0)))
		#f)))

(define* (zfs-create-snapshot buckets bucket delta-time)
  (let ((last-snapshot (zfs-get-newest-snapshot buckets bucket))
		(now (get-current-timestamp)))
	(if (>= (- now (assoc-ref last-snapshot 'creation)) delta-time)
		(let* ((dataset (assoc-ref last-snapshot 'dataset))
			   (new-name (format #f "~a@autosnap_~d_~a" dataset now bucket)))
		  (zfs-run-command (string-append "snapshot " new-name))
		  (zfs-add-entry-to-bucket buckets
								   bucket
								   (zfs-parse-snapshot-name dataset new-name ""))
		  (format #t
				  "will create a new snapshot for bucket ~a (~a; ~d ~d)\n"
				  bucket new-name now (assoc-ref last-snapshot 'creation))))))

(define (zfs-get-newest-snapshot buckets bucket)
  (let ((items (assoc-ref buckets bucket)))	
	(car (sort-list items
					(lambda (a b)
					  (>= (assoc-ref a 'creation) (assoc-ref b 'creation)))))))

(define (zfs-get-oldest-snapshot buckets bucket)
  (let ((items (assoc-ref buckets bucket)))	
	(car (sort-list items
					(lambda (a b)
					  (<= (assoc-ref a 'creation) (assoc-ref b 'creation)))))))

(define (zfs-apply-policy)
  (load (string-append (getenv "HOME") "/.zbkp-config.scm"))
  (for-each (lambda (dataset-config)
			  (let* ((dataset (assoc-ref dataset-config 'name))
					 (buckets (zfs-get-all-snapshots dataset))
					 (backup-server (assoc-ref dataset-config 'backup-server))
					 ;; (remote-buckets
					 ;;  (cond (backup-server
					 ;; 		 (zfs-get-all-snapshots (assoc-ref backup-server 'dest-dataset)
					 ;; 								(assoc-ref backup-server 'ssh-string)))
					 ;; 		(else '())))
					 (no-create (or (assoc-ref dataset-config 'no-create) #f))
					 (hourly-limit (assoc-ref dataset-config 'hourly-limit))
					 (daily-limit (assoc-ref dataset-config 'daily-limit))
					 (monthly-limit (assoc-ref dataset-config 'monthly-limit)))
				(if (not no-create)
					(for-each (lambda (x) (zfs-create-snapshot buckets (car x) (cadr x)))
							  `((hourly ,(* 60 60)) (daily ,(* 60 60 24)) (monthly ,(* 60 60 24 30)))))
				(for-each (lambda (x) (zfs-trim-bucket buckets (car x) (cadr x) no-create))
						  `((hourly ,hourly-limit) (daily ,daily-limit) (monthly ,monthly-limit)))
				(zfs-backup-all dataset backup-server)))
			zfs-policy-datasets))

(define* (zfs-backup-entry entry server-config #:optional previous)
  "Creates a backup by sending the snapshot to a remote
destination. If the backup succeeds, marks the snapshot as backed up,
so it can be deleted. If a previous entry is passed, the send is incremental"
  (let* ((server (assoc-ref server-config 'ssh-string))
		 (dest-dataset (assoc-ref server-config 'dest-dataset))
		 (name (assoc-ref entry 'name))
		 (prev-name (if previous (format #f "-i ~a" (assoc-ref previous 'name)) ""))
		 (pipe-string (format #f "send ~a ~a | ~a sudo zfs recv ~a"
							  prev-name name server dest-dataset))
		 (result (zfs-run-command pipe-string)))
	(if (eq? result '()) (zfs-run-command (format #f "set 'zbkp:backed-up=true' ~a" name)))))

(define* (zfs-get-all-snapshots-no-bucket dataset #:optional (ssh-string ""))
  (let ((output (zfs-run-command
				 (format #f "list -rHt snapshot ~a" dataset) ssh-string))
		(buckets '()))
	(filter (lambda (x) ((negate not) x))
			(map
			 (lambda (row)
			   ;; the first item is the name, try to match it to our pattern:
			   ;; <dataset>@autosnap_<timestamp>_<bucket>
			   (let* ((md (zfs-match-snapshot-name (car row)))
					  (snapshot (and md (zfs-parse-snapshot-name dataset (car row) ssh-string))))
				 (or snapshot #f)))
			 output))))

(define (zfs-backup-all dataset server-config)
  (let* ((all (zfs-get-all-snapshots-no-bucket dataset))
		 (prev #f)
		 (server (assoc-ref server-config 'ssh-string)))
	(if (zfs-can-connect server)
		(for-each
		 (lambda (entry)
		   (let* ((name (assoc-ref entry 'name))
				  (backed-up (assoc-ref entry 'backed-up)))
			 (format #t "will try to backup ~a (~a; prev: ~a)\n" name backed-up (assoc-ref prev 'name))
			 (if (eqv? backed-up #f) (zfs-backup-entry entry server-config prev))
			 (set! prev entry)))
		 all))))

(define (zfs-get-used dataset ssh-string)
  (let ((output (zfs-run-command (string-append "get -Hp used " dataset) ssh-string)))
	(cond
	 ;; only one line, and the first one has 4 items
	 ((and (equal? (length output) 1) (equal? (length (car output)) 4))
	  (string->number (zfs-get-field (car output) 2)))
	 (else #f))))

(define (zfs-get-creation dataset ssh-string)
  (let ((output (zfs-run-command (string-append "get -Hp creation " dataset) ssh-string)))
	(cond
	 ((and (equal? (length output) 1) (equal? (length (car output)) 4))
	  (string->number (zfs-get-field (car output) 2)))
	 (else #f))))

(define (zfs-get-backed-up dataset ssh-string)
  (let ((output (zfs-run-command
				 (string-append "get -Hp zbkp:backed-up " dataset) ssh-string)))
	(cond
	 ((and (equal? (length output) 1) (equal? (length (car output)) 4))
	  (string=? (zfs-get-field (car output) 2) "true"))
	 (else #f))))

(define epoch (date->time-utc (make-date 0 0 0 0 1 1 1970 0)))

(define (get-current-timestamp)
  (time-second (time-difference (current-time) epoch)))

(define (parse-timestamp seconds)
  (time-utc->date (add-duration epoch (make-time 'time-duration 0 seconds)) 0))

(define (date->timestamp date)
  (time-second (time-difference (date->time-utc date) epoch)))

(define zfs-get-field
  (lambda (zfs-row field)
	(vector-ref (list->vector zfs-row) field)))

;; main entry point, we can prob parse arguments here if we wanted
(let ()
  (zfs-apply-policy))
