FROM debian:latest
RUN apt-get update && apt-get -y install emacs-nox git
RUN emacs --batch --eval "(progn (setq package-archives '((\"gnu\" . \"http://elpa.gnu.org/packages/\") (\"melpa\" . \"http://melpa.org/packages/\") (\"org\" . \"http://orgmode.org/elpa/\"))) (package-initialize) (package-refresh-contents) (package-install 'org) (package-install 'htmlize))"
